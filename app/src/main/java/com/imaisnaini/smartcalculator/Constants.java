package com.imaisnaini.smartcalculator;

public class Constants {
    public static final String PREFERENCE_FILE_NAME = "smart-calculator-prefs";
    public static final String IS_FIRST_LAUNCH = "isFirstLaunch";
    public static final String PREF_INSTALLATION_UUID = "installationUuid";
    public static final String DATABASE_NAME = "smartcalculator01";
    public static final String STORAGE_NAME = "smart_calculator";
    public static final String PREF_STORAGE = "storageEngine";
}
