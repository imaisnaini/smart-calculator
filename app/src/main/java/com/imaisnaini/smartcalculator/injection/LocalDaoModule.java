package com.imaisnaini.smartcalculator.injection;

import android.content.Context;

import com.imaisnaini.smartcalculator.data.local.AppDatabase;
import com.imaisnaini.smartcalculator.data.local.dao.CalculationDao;
import com.imaisnaini.smartcalculator.data.local.dao.DeviceLogDao;
import com.imaisnaini.smartcalculator.data.local.dao.FileContentDao;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.qualifiers.ApplicationContext;
import dagger.hilt.components.SingletonComponent;

@Module
@InstallIn(SingletonComponent.class)
public class LocalDaoModule {
    @Provides
    public static FileContentDao fileContentDao(@ApplicationContext Context ctx) {
        return AppDatabase.getInstance(ctx).fileContentDao();
    }
    @Provides
    public static CalculationDao calculationDao(@ApplicationContext Context ctx) {
        return AppDatabase.getInstance(ctx).calculationDao();
    }
    @Provides
    public static DeviceLogDao deviceLogDao(@ApplicationContext Context ctx) {
        return AppDatabase.getInstance(ctx).deviceLogDao();
    }
}
