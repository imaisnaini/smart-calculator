package com.imaisnaini.smartcalculator.util;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.auto.service.AutoService;
import com.imaisnaini.smartcalculator.BuildConfig;
import com.imaisnaini.smartcalculator.data.local.AppDatabase;
import com.imaisnaini.smartcalculator.data.local.dao.DeviceLogDao;
import com.imaisnaini.smartcalculator.data.local.entity.DeviceLogL;

import org.acra.ReportField;
import org.acra.config.CoreConfiguration;
import org.acra.data.CrashReportData;
import org.acra.sender.ReportSender;
import org.acra.sender.ReportSenderException;
import org.acra.sender.ReportSenderFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import timber.log.Timber;

public class CrashReporter implements ReportSender {

    private Context context;
    public CrashReporter(Context context, CoreConfiguration config) {
        this.context = context;
    }

    @Override
    public void send(@NonNull Context context, @NonNull CrashReportData errorContent, @NonNull Bundle extras) throws ReportSenderException {
        try {
            Object custom = errorContent.getString(ReportField.CUSTOM_DATA);
            ObjectMapper mapper = new ObjectMapper();
            String jsonInput = custom.toString();
            TypeReference<HashMap<String, String>> typeRef = new TypeReference<HashMap<String, String>>() {};
            Map<String, String> map = mapper.readValue(jsonInput, typeRef);

            DeviceLogL log = new DeviceLogL(BuildConfig.APPLICATION_ID, BuildConfig.GIT_HASH);
            log.eventType = DeviceLogL.EV_CRASH;

            String installationId = map.get("installationId");
            String userId = map.get("userId");
            if(installationId != null) {
                log.installationId = UUID.fromString(installationId);
            }
            log.description = errorContent.getString(ReportField.STACK_TRACE);

            AppDatabase db = AppDatabase.getInstance(context);
            DeviceLogDao dao = db.deviceLogDao();

            db.runInTransaction(() -> {
                dao.blockingInsert(log);
            });
        } catch (JsonProcessingException e) {
            Timber.e(e);
        }
    }

    @AutoService(ReportSenderFactory.class)
    public static class CrashReporterFactory implements ReportSenderFactory {

        @NonNull
        @Override
        public ReportSender create(@NonNull Context context, @NonNull CoreConfiguration coreConfiguration) {
            return new CrashReporter(context, coreConfiguration);
        }

        @Override
        public boolean enabled(@NonNull CoreConfiguration config) {
            return true;
        }
    }
}
