package com.imaisnaini.smartcalculator.util;

public class PermissionRequestDto {
    private String permission;
    private String rationale;

    public PermissionRequestDto(String permission, String rationale) {
        this.permission = permission;
        this.rationale = rationale;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getRationale() {
        return rationale;
    }

    public void setRationale(String rationale) {
        this.rationale = rationale;
    }
}
