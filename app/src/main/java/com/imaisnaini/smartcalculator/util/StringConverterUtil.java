package com.imaisnaini.smartcalculator.util;

import android.text.TextUtils;

public class StringConverterUtil {
    public static double str2double(String val) {
        double toReturn = 0.0;
        try {
            if(!TextUtils.isEmpty(val)) {
                toReturn = Double.parseDouble(val);
            }
        } catch(NumberFormatException exc) {
            exc.printStackTrace();
        }
        return toReturn;
    }

    public static float str2float(String val){
        float toReturn = 0f;
        try {
            if(!TextUtils.isEmpty(val)) {
                toReturn = Float.parseFloat(val);
            }
        } catch(NumberFormatException exc) {
            exc.printStackTrace();
        }
        return toReturn;
    }

    public static int str2int(String val){
        int toReturn = 0;
        try {
            if(!TextUtils.isEmpty(val)) {
                toReturn = Integer.parseInt(val);
            }
        } catch(NumberFormatException exc) {
            exc.printStackTrace();
        }
        return toReturn;
    }
}
