package com.imaisnaini.smartcalculator.util;

import android.content.Context;
import android.security.keystore.KeyGenParameterSpec;

import androidx.security.crypto.EncryptedFile;
import androidx.security.crypto.MasterKeys;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.GeneralSecurityException;

/**
 * Utility class to implement file encryption in Android, using Jetpack Security library.
 * Please see https://developer.android.com/topic/security/data.
 */
public class FileEncryptionUtil {

    private FileEncryptionUtil() {
        // hide constructor
    }

    public static EncryptedFile obtainEncryptedFile(Context context, String filePath) throws GeneralSecurityException, IOException {
        KeyGenParameterSpec keyGenParameterSpec = MasterKeys.AES256_GCM_SPEC;
        String mainKeyAlias = MasterKeys.getOrCreate(keyGenParameterSpec);
        EncryptedFile encryptedFile = new EncryptedFile.Builder(
                new File(filePath),
                context,
                mainKeyAlias,
                EncryptedFile.FileEncryptionScheme.AES256_GCM_HKDF_4KB
        ).build();
        return encryptedFile;
    }

    public static byte[] decryptFileToMemory(Context context, String filePath) throws GeneralSecurityException, IOException {
        EncryptedFile decryptedFile = obtainEncryptedFile(context, filePath);
        ByteArrayOutputStream baosDecrypted = new ByteArrayOutputStream(65536);
        byte[] readBuffer = new byte[1024];
        try (FileInputStream decryptedInputStream = decryptedFile.openFileInput()) {
            int len = decryptedInputStream.read(readBuffer);
            while (len != -1) {
                baosDecrypted.write(readBuffer, 0, len);
                len = decryptedInputStream.read(readBuffer);
            }
        }
        byte[] rawBytes = baosDecrypted.toByteArray();
        return rawBytes;
    }

    public static void decryptFileToFile(Context context, String sourcePath, String destinationPath) throws GeneralSecurityException, IOException {
        EncryptedFile encryptedFile = obtainEncryptedFile(context, sourcePath);
        byte[] readBuffer = new byte[1024];
        try (FileInputStream fis = encryptedFile.openFileInput();
             FileOutputStream fos = new FileOutputStream(destinationPath)
        ) {
            int len = fis.read(readBuffer);
            while (len != -1) {
                fos.write(readBuffer, 0, len);
                len = fis.read(readBuffer);
            }
            fos.flush();
        }
    }

    public static void encryptFileToFile(Context context, String sourcePath, String destinationPath) throws GeneralSecurityException, IOException {
        EncryptedFile encryptedFile = obtainEncryptedFile(context, destinationPath);
        byte[] readBuffer = new byte[1024];
        try (FileInputStream fis = new FileInputStream(sourcePath);
             OutputStream fos = encryptedFile.openFileOutput()
        ) {
            int len = fis.read(readBuffer);
            while (len != -1) {
                fos.write(readBuffer, 0, len);
                len = fis.read(readBuffer);
            }
            fos.flush();
        }
    }

    public static void encryptMemoryToFile(Context context, byte[] plainText, String destinationPath) throws GeneralSecurityException, IOException {
        EncryptedFile encryptedFile = obtainEncryptedFile(context, destinationPath);
        try (OutputStream fos = encryptedFile.openFileOutput()) {
            fos.write(plainText);
            fos.flush();
        }
    }
}
