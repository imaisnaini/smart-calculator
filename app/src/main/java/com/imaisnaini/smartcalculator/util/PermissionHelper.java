package com.imaisnaini.smartcalculator.util;

import android.content.Context;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;

import timber.log.Timber;

public class PermissionHelper {
    private static String TAG = PermissionHelper.class.getName();

    public static boolean hasPermission(Context ctx, String permission) {
        int permissionState = ActivityCompat.checkSelfPermission(ctx, permission);
        boolean hasPermission = (permissionState == PackageManager.PERMISSION_GRANTED);
        Timber.d( "Has %s permission ? %s", permission, hasPermission ? "TRUE" : "FALSE");
        return hasPermission;
    }

    public static PermissionRequestDto createRequest(String permission, String rationale) {
        return new PermissionRequestDto(permission,rationale);
    }

}

