package com.imaisnaini.smartcalculator

import dagger.hilt.android.HiltAndroidApp
import android.app.Application
import android.content.Context
import androidx.camera.lifecycle.ProcessCameraProvider
import javax.inject.Inject
import androidx.hilt.work.HiltWorkerFactory
import timber.log.Timber
import timber.log.Timber.DebugTree
import org.acra.config.CoreConfigurationBuilder
import org.acra.data.StringFormat
import com.github.f4b6a3.uuid.UuidCreator
import android.os.Build
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.core.JsonProcessingException
import android.util.Log
import androidx.work.*
import com.imaisnaini.smartcalculator.data.local.AppDatabase
import com.imaisnaini.smartcalculator.data.local.entity.DeviceLogL
import com.imaisnaini.smartcalculator.util.CrashLogger
import org.acra.ACRA.errorReporter
import org.acra.ACRA.init
import java.lang.Exception
import java.lang.StringBuilder
import java.util.*
import java.util.concurrent.ExecutionException
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

@HiltAndroidApp
class SmartCalculatorApplication : Application(), Configuration.Provider {
    private lateinit var cameraProvider: ProcessCameraProvider
    private lateinit var executorService: ExecutorService

    @Inject
    lateinit var workerFactory: HiltWorkerFactory
    private var installationUuid: String? = null

    override fun getWorkManagerConfiguration(): Configuration {
        return Configuration.Builder()
            .setWorkerFactory(workerFactory!!)
            .build()
    }

    /** A tree which logs important information for crash reporting.  */
    private class CrashReportingTree : Timber.Tree() {
        override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
            if (priority == Log.VERBOSE || priority == Log.DEBUG) {
                return
            }
            CrashLogger.log(priority, tag, message)
            if (t != null) {
                if (priority == Log.ERROR) {
                    CrashLogger.logError(t)
                } else if (priority == Log.WARN) {
                    CrashLogger.logWarning(t)
                }
            }
        }
    }

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        } else {
            Timber.plant(CrashReportingTree())
        }
        executorService = Executors.newFixedThreadPool(3)
        executorService.submit(Runnable {
            generateInstallationUuid()
            setupCameraProvider()
        })
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        val builder = CoreConfigurationBuilder()
        //core configuration:
        builder
            .withBuildConfigClass(BuildConfig::class.java)
            .withReportFormat(StringFormat.KEY_VALUE_LIST)
        //        builder.withPluginConfigurations()
        init(this, builder)
    }

    /**
     * This ID to differentiate between application installation across devices and time.
     * When this application un-installed and installed again within a single device,
     * the installation UUID would be different.
     */
    private fun generateInstallationUuid() {
        try {
            val pref = getSharedPreferences(Constants.PREFERENCE_FILE_NAME, MODE_PRIVATE)
            installationUuid = pref.getString(Constants.PREF_INSTALLATION_UUID, null)
            if (installationUuid == null) {
                installationUuid = UuidCreator.getTimeBasedWithMac().toString()
                pref.edit().putString(Constants.PREF_INSTALLATION_UUID, installationUuid).apply()
                createInstallationLog(installationUuid!!)
            }
            errorReporter.putCustomData("installationId", installationUuid!!)
            val uuidInstallation = UUID.fromString(installationUuid)
            val node = uuidInstallation.node()
            System.setProperty("uuidcreator.node", String.format(Locale.getDefault(), "%d", node))
            Timber.i("Startup %s, %d", installationUuid, node)
        } catch (exc: Exception) {
            Timber.e(exc)
        }
    }

    private fun createInstallationLog(installationId: String) {
        try {
            val log = DeviceLogL(BuildConfig.APPLICATION_ID, BuildConfig.GIT_HASH)
            log.eventType = DeviceLogL.EV_INSTALL
            log.installationId = UUID.fromString(installationId)
            val sb = StringBuilder(512)
            sb.append(
                String.format(
                    Locale.getDefault(),
                    "Android API Level: %s\n",
                    Build.VERSION.SDK_INT
                )
            )
            sb.append(String.format(Locale.getDefault(), "Manufacturer: %s\n", Build.MANUFACTURER))
            sb.append(String.format(Locale.getDefault(), "Board: %s\n", Build.BOARD))
            sb.append(String.format(Locale.getDefault(), "Hardware: %s\n", Build.HARDWARE))
            sb.append(String.format(Locale.getDefault(), "Brand: %s\n", Build.BRAND))
            sb.append(String.format(Locale.getDefault(), "Model: %s\n", Build.MODEL))
            sb.append(String.format(Locale.getDefault(), "Product: %s\n", Build.PRODUCT))
            sb.append(String.format(Locale.getDefault(), "Device: %s\n", Build.DEVICE))
            sb.append(String.format(Locale.getDefault(), "Fingerprint: %s\n", Build.FINGERPRINT))
            sb.append(
                String.format(
                    Locale.getDefault(),
                    "App Build: %s\n",
                    if (BuildConfig.DEBUG) "Debug" else "Release"
                )
            )
            log.description = sb.toString()
            log.createdBy = "SYSTEM"
            log.updatedBy = "SYSTEM"
            val om = ObjectMapper()
            val db = AppDatabase.getInstance(applicationContext)
            val devDao = db.deviceLogDao()
            db.runInTransaction {
                devDao.blockingInsert(log)
            }
        } catch (e: JsonProcessingException) {
            Timber.e(e)
        }
    }

    private fun setupCameraProvider() {
        try {
            val future = ProcessCameraProvider.getInstance(applicationContext)
            future.addListener({
                try {
                    cameraProvider = future.get()
                } catch (e: ExecutionException) {
                    e.printStackTrace()
                } catch (e: InterruptedException) {
                    Timber.e(e, "Got exception while getting ProcessCameraProvider")
                    Thread.currentThread().interrupt()
                }
            }, executorService)
        } catch (exc: Exception) {
            Timber.e(exc)
        }
    }
}