package com.imaisnaini.smartcalculator.ui

import CalculatorTheme
import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.google.common.util.concurrent.ListenableFuture
import com.google.mlkit.vision.common.InputImage
import com.imaisnaini.smartcalculator.R
import com.imaisnaini.smartcalculator.util.CameraUtils
import com.imaisnaini.smartcalculator.util.PermissionHelper
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import java.util.concurrent.Executor
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

@AndroidEntryPoint
class CameraScreenFragment : Fragment() {
    private val mViewModel : MainScreenViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        checkAndRequestCameraPermission()

        return ComposeView(requireContext()).apply {
            setContent {
                val navOptions: NavOptions = NavOptions
                    .Builder()
                    .setPopUpTo(findNavController().graph.startDestinationId, true)
                    .build()
                val bundle = Bundle()
                val imageCapture: MutableState<ImageCapture?> = remember { mutableStateOf(null) }
                val executor = remember(context) { ContextCompat.getMainExecutor(context) }

                mViewModel.getImageOutputLiveData().observe(viewLifecycleOwner){
                    if(it != null) {
                        Toast.makeText(requireContext(), it.input, Toast.LENGTH_SHORT).show()
                    }
                }
                CalculatorTheme {
                    Surface {
                        Box {
                            Surface(
                                modifier = Modifier
                                    .fillMaxSize()
                                    .systemBarsPadding()
                            ) {
                                Column(horizontalAlignment = Alignment.CenterHorizontally,
                                    modifier = Modifier.fillMaxSize()) {
                                    TopBar()
                                    Column(modifier = Modifier
                                        .weight(1f, true)
                                        .padding(all = dimensionResource(id = R.dimen.padding_normal))) {
                                        CameraPreview(imageCapture, executor)
                                    }
                                    OutlinedButton(onClick = {
                                        imageCapture.value?.takePicture(executor,
                                            object : ImageCapture.OnImageCapturedCallback() {
                                                override fun onCaptureSuccess(image: ImageProxy) {
                                                    val bitmap = CameraUtils.convertImageProxyToBitmap(
                                                        image,
                                                        requireActivity()
                                                    )
                                                    mViewModel.setImageLiveData(bitmap)
                                                    findNavController().navigate(R.id.navResultScreenFragment, bundle, navOptions)
                                                }

                                                override fun onError(exception: ImageCaptureException) {
                                                    Toast.makeText(context, exception.message, Toast.LENGTH_LONG)
                                                        .show()
                                                }
                                            })
                                                             },
                                    modifier = Modifier.padding(
                                        horizontal = dimensionResource(id = R.dimen.padding_normal),
                                        vertical = dimensionResource(id = R.dimen.padding_large)
                                    )) {
                                        Text(getString(R.string.btn_capture))
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Composable
    private fun TopBar(){
        TopAppBar (
            title = { Row(verticalAlignment = Alignment.CenterVertically) {
                Image(
                    painterResource(R.drawable.ic_logo),
                    contentDescription = "Logo",
                    contentScale = ContentScale.Inside,
                    modifier = Modifier
                        .height(dimensionResource(id = R.dimen.icon_small))
                )
                Text(
                    stringResource(R.string.camera_fragment),
                    modifier = Modifier.padding(horizontal = dimensionResource(id = R.dimen.padding_normal)))
            } },
            elevation = 0.dp,
            navigationIcon = {
                IconButton(onClick = { findNavController().popBackStack() }) {
                    Icon(
                        imageVector = Icons.Filled.ArrowBack,
                        contentDescription = "Back to previous page"
                    )
                }
            }
        )
        Divider()
    }

    @Composable
    fun CameraPreview(imageCapture: MutableState<ImageCapture?>,
                      executor: Executor,
    ) {
        val context = LocalContext.current
        val lifecycleOwner = LocalLifecycleOwner.current
        val previewCameraView = remember { PreviewView(context) }

        AndroidView(
            factory = { AndroidViewContext ->
                PreviewView(AndroidViewContext).apply {
                    this.scaleType = PreviewView.ScaleType.FILL_CENTER
                    layoutParams = ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT,
                    )
                    implementationMode = PreviewView.ImplementationMode.COMPATIBLE
                }
            },
            modifier = Modifier
                .fillMaxSize()
                .padding(vertical = dimensionResource(id = R.dimen.padding_normal)),
            update = { previewView ->
                val cameraSelector: CameraSelector = CameraSelector.Builder()
                    .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                    .build()
                val cameraExecutor: ExecutorService = Executors.newSingleThreadExecutor()
                val cameraProviderFuture: ListenableFuture<ProcessCameraProvider> =
                    ProcessCameraProvider.getInstance(context)
                imageCapture.value = ImageCapture.Builder().build()

                cameraProviderFuture.addListener({
                    val preview = Preview.Builder().build().also {
                        it.setSurfaceProvider(previewView.surfaceProvider)
                    }
                    val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()

                    try {
                        cameraProvider.unbindAll()
                        cameraProvider.bindToLifecycle(
                            lifecycleOwner,
                            cameraSelector,
                            imageCapture.value,
                            preview
                        )
                    } catch (e: Exception) {
                        Timber.d("CameraPreview: ${e.localizedMessage}")
                    }
                }, executor)
                previewCameraView
            }
        )
    }

    private fun checkAndRequestCameraPermission() {
        if (PermissionHelper.hasPermission(requireContext(), Manifest.permission.CAMERA)
        ) {
            Timber.i("CAMERA permission granted")

        } else if (ActivityCompat.shouldShowRequestPermissionRationale(
                requireActivity(),
                Manifest.permission.CAMERA
            )
        ) {
            Timber.i("Permission Rationale is enforced")
            com.google.android.material.snackbar.Snackbar.make(
                requireView(),
                getString(R.string.camera_permissiom_request),
                com.google.android.material.snackbar.Snackbar.LENGTH_INDEFINITE
            )
                .setAction("OK") { v: View? ->
                    Timber.i("Launch permission request screen after showing rationale")
                    cameraPermissionLauncher.launch(Manifest.permission.CAMERA)
                }
                .show()
        } else {
            Timber.i("Launch permission request screen")
            cameraPermissionLauncher.launch(Manifest.permission.CAMERA)
        }
    }

    val cameraPermissionLauncher = registerForActivityResult<String, Boolean>(
        ActivityResultContracts.RequestPermission()
    ) { isGranted: Boolean ->
        if (isGranted) {
            Timber.i("CAMERA permission granted")
            Toast.makeText(context, "Permission granted", Toast.LENGTH_SHORT).show()
        } else {
            //permission was denied
            Toast.makeText(context, "Permission denied...!", Toast.LENGTH_SHORT).show()
        }
    }
}