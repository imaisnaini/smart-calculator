package com.imaisnaini.smartcalculator.ui

import android.app.Application
import android.graphics.Bitmap
import androidx.compose.runtime.MutableState
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.TextRecognition
import com.google.mlkit.vision.text.latin.TextRecognizerOptions
import com.imaisnaini.smartcalculator.data.domain.entity.Calculation
import com.imaisnaini.smartcalculator.data.domain.usecase.CreateCalculationUseCase
import com.imaisnaini.smartcalculator.data.domain.usecase.GetCalculationHistoriesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class MainScreenViewModel @Inject constructor(
    application: Application,
    private val getCalculationHistoriesUseCase: GetCalculationHistoriesUseCase,
    private var createCalculationUseCase: CreateCalculationUseCase
) : ViewModel() {
    val recognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS)

    // observable
    private var calculationsLiveData = MutableLiveData<List<Calculation>>()
    fun getCalculationsLiveData() = calculationsLiveData

    private var showDialog = MutableLiveData(true)
    fun getShowDialog() = showDialog

    private var imageLiveData = MutableLiveData<Bitmap>()
    fun getImageLiveData() = imageLiveData
    fun setImageLiveData(bitmap: Bitmap){ imageLiveData.value = bitmap}

    private var imageOutputLiveData = MutableLiveData<Calculation>()
    fun getImageOutputLiveData() = imageOutputLiveData

    fun reload(){
        showDialog.value = true
        Single.fromCallable{ getCalculationHistoriesUseCase.invokeSync() }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribe({
                if (it.success) {
                    calculationsLiveData.postValue(it.records)
                }
                showDialog.postValue(false)
            }, { t: Throwable? -> Timber.e(t) })
    }

    fun scan(bitmap: Bitmap){
        showDialog.value = true
        val image = InputImage.fromBitmap(bitmap, 0)
        val result = recognizer.process(image)
            .addOnSuccessListener { visionText ->
                // Task completed successfully
                // [START_EXCLUDE]
                // [START get_text]
                for (block in visionText.textBlocks) {
                    val boundingBox = block.boundingBox
                    val cornerPoints = block.cornerPoints
                    val text = block.text

                    for (line in block.lines) {
                        // ...
                        for (element in line.elements) {
                            // ...
                        }
                    }
                }
//                imageOutputLiveData.value = visionText.text
                Timber.i("Image Output : %s", visionText.text)
                Single.fromCallable{ createCalculationUseCase.invokeSync(bitmap, visionText.text) }
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribe({
                        if (it.success) {
                            imageOutputLiveData.postValue(it.record)
                        }
                        showDialog.postValue(false)
                    }, { t: Throwable? -> Timber.e(t) })
                // [END get_text]
                // [END_EXCLUDE]
            }
            .addOnFailureListener { e ->
                // Task failed with an exception
                // ...
            }
    }
}