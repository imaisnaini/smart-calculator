package com.imaisnaini.smartcalculator.ui

import CalculatorTheme
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.google.mlkit.vision.common.InputImage
import com.imaisnaini.smartcalculator.R
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class ResultScreenFragment : Fragment() {
    private val mViewModel : MainScreenViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {
                val bitmap : MutableState<Bitmap?> = remember { mutableStateOf(null) }
                mViewModel.getImageLiveData().observe(viewLifecycleOwner){
                    if (it != null){
                        bitmap.value = it
                        mViewModel.scan(it)
                    }
                }
                CalculatorTheme {
                    Surface {
                        Box {
                            Surface(
                                modifier = Modifier
                                    .fillMaxSize()
                                    .systemBarsPadding()
                            ) {
                                Column(
                                    modifier = Modifier.fillMaxSize()
                                ) {
                                    TopBar()
                                    Column(
                                        horizontalAlignment = Alignment.CenterHorizontally,
                                        modifier = Modifier
                                            .fillMaxWidth()
                                            .weight(4f, true)
                                            .padding(all = dimensionResource(id = R.dimen.padding_normal))) {
                                        mViewModel.getImageLiveData().observeAsState().value?.let {
                                            Image(it.asImageBitmap(), contentDescription = "Image captured",
                                            contentScale = ContentScale.Fit,modifier = Modifier.fillMaxSize())
                                        }
                                    }
                                    Column(verticalArrangement = Arrangement.Center,
                                        horizontalAlignment = Alignment.CenterHorizontally,
                                        modifier = Modifier
                                            .weight(1f, true)
                                            .fillMaxWidth()
                                            .padding(horizontal = dimensionResource(id = R.dimen.padding_normal))) {
                                        Text("Input : " + (mViewModel.getImageOutputLiveData().observeAsState().value?.input
                                            ?: "Unidentified Input"),modifier = Modifier.fillMaxWidth())
                                        Text(String.format(Locale.getDefault(), "Output : %.2f", mViewModel.getImageOutputLiveData().observeAsState().value?.result),
                                        modifier = Modifier.fillMaxWidth())
                                        Button(onClick = {findNavController().navigateUp() },
                                        modifier = Modifier.padding(all = dimensionResource(id = R.dimen.padding_normal))) {
                                            Text(getString(R.string.btn_close))
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Composable
    private fun TopBar(){
        TopAppBar (
            title = { Row(verticalAlignment = Alignment.CenterVertically) {
                Image(
                    painterResource(R.drawable.ic_logo),
                    contentDescription = "Logo",
                    contentScale = ContentScale.Inside,
                    modifier = Modifier
                        .height(dimensionResource(id = R.dimen.icon_small))
                )
                Text(
                    stringResource(R.string.result_fragment),
                    modifier = Modifier.padding(horizontal = dimensionResource(id = R.dimen.padding_normal)))
            } },
            elevation = 0.dp,
            navigationIcon = {
                IconButton(onClick = { findNavController().navigateUp() }) {
                    Icon(
                        imageVector = Icons.Filled.ArrowBack,
                        contentDescription = "Back to previous page"
                    )
                }
            }
        )
        Divider()
    }
}