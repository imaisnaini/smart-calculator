package com.imaisnaini.smartcalculator.ui.theme

import androidx.compose.ui.graphics.Color

val Rose11 = Color(0xff7f0054)
val Rose10 = Color(0xff97005c)
val Rose9 = Color(0xffaf0060)
val Rose8 = Color(0xffc30060)
val Rose7 = Color(0xffd4005d)
val Rose6 = Color(0xffe21365)
val Rose5 = Color(0xffec3074)
val Rose4 = Color(0xfff4568b)
val Rose3 = Color(0xfff985aa)
val Rose2 = Color(0xfffdbbcf)
val Rose1 = Color(0xfffed6e2)
val Rose0 = Color(0xfffff2f6)

val Leaf11 = Color(0xFF005600)
val Leaf10 = Color(0xFF006D00)
val Leaf9 = Color(0xFF008700)
val Leaf8 = Color(0xFF00A100)
val Leaf7 = Color(0xFF00B900)
val Leaf6 = Color(0xFF13D013)
val Leaf5 = Color(0xFF30E230)
val Leaf4 = Color(0xFF57EF57)
val Leaf3 = Color(0xFF86F786)
val Leaf2 = Color(0xFFBBFDBB)
val Leaf1 = Color(0xFFD6FED6)
val Leaf0 = Color(0xFFF2FFF2)

val Neutral8 = Color(0xff121212)
val Neutral7 = Color(0xde000000)
val Neutral6 = Color(0x99000000)
val Neutral5 = Color(0x61000000)
val Neutral4 = Color(0x1f000000)
val Neutral3 = Color(0x1fffffff)
val Neutral2 = Color(0x61ffffff)
val Neutral1 = Color(0xbdffffff)
val Neutral0 = Color(0xffffffff)

val FunctionalRed = Color(0xffd00036)
val FunctionalRedDark = Color(0xffea6d7e)
val FunctionalGreen = Color(0xff52c41a)
val FunctionalGrey = Color(0xfff6f6f6)
val FunctionalDarkGrey = Color(0xff2e2e2e)

const val AlphaNearOpaque = 0.95f