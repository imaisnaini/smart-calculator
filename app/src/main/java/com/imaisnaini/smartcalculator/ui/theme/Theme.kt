import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import com.imaisnaini.smartcalculator.BuildConfig
import com.imaisnaini.smartcalculator.ui.theme.*

private val DarkColorPalette = darkColors(
    primary = Rose6,
    primaryVariant = Rose9,
    onPrimary = Neutral0,
    secondary = Rose3,
    secondaryVariant = Rose7,
    onSecondary = Neutral7
)

private val GreenLightColorPalette = lightColors(
    primary = Leaf6,
    primaryVariant = Leaf9,
    onPrimary = Neutral0,
    secondary = Leaf3,
    secondaryVariant = Leaf7,
    onSecondary = Neutral7
)

private val RedLightColorPalette = lightColors(
    primary = Rose7,
    primaryVariant = Rose10,
    onPrimary = Neutral0,
    secondary = Rose4,
    secondaryVariant = Rose8,
    onSecondary = Neutral7
)

@Composable
fun CalculatorTheme(darkTheme: Boolean = isSystemInDarkTheme(), content: @Composable () -> Unit) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        if (BuildConfig.FLAVOR_color.equals("green")){
            GreenLightColorPalette
        }else{
            RedLightColorPalette
        }
    }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}