package com.imaisnaini.smartcalculator.ui

import CalculatorTheme
import android.Manifest
import android.app.Application
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.ImageDecoder
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.selection.selectableGroup
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.imaisnaini.smartcalculator.BuildConfig
import com.imaisnaini.smartcalculator.Constants
import com.imaisnaini.smartcalculator.R
import com.imaisnaini.smartcalculator.data.domain.entity.Calculation
import com.imaisnaini.smartcalculator.util.FileEncryptionUtil
import com.imaisnaini.smartcalculator.util.PermissionHelper
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import java.io.IOException
import java.nio.file.Paths
import java.security.GeneralSecurityException

@AndroidEntryPoint
class MainScreenFragment : Fragment() {
    private val mViewModel : MainScreenViewModel by activityViewModels()
    private lateinit var pref: SharedPreferences
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mViewModel.reload()
        if (BuildConfig.FLAVOR_engine.equals("filesystem")){
            checkAndRequestStoragePermission()
        }
        pref = requireContext().getSharedPreferences(Constants.PREFERENCE_FILE_NAME, Application.MODE_PRIVATE)
        if (pref.getString(Constants.PREF_STORAGE, "") == null) {
            pref.edit().putString(Constants.PREF_STORAGE, "Use file storage").apply()
        }

        return ComposeView(requireContext()).apply {
            setContent {
                val systemEngine = remember {
                    mutableStateOf(pref.getString(Constants.PREF_STORAGE, null))
                }
                val calculationHistores = remember {
                    mutableStateListOf<Calculation>()
                }
                val showDialog = remember {
                    mutableStateOf(true)
                }
                val pickPictureLauncher = rememberLauncherForActivityResult(
                    ActivityResultContracts.GetContent()
                ) { imageUri ->
                    if (imageUri != null) {
                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                            val source = ImageDecoder.createSource(requireContext().contentResolver, imageUri)
                            val bitmap = ImageDecoder.decodeBitmap(source)
                            mViewModel.setImageLiveData(bitmap)
                        } else {
                            val bitmap = MediaStore.Images.Media.getBitmap(
                                requireContext().contentResolver,
                                imageUri
                            )
                            mViewModel.setImageLiveData(bitmap)
                        }
                        findNavController().navigate(R.id.navResultScreenFragment)
                    }
                }
                mViewModel.getCalculationsLiveData().observe(viewLifecycleOwner){
                    calculationHistores.apply {
                        clear()
                        addAll(it)
                    }
                }
                mViewModel.getShowDialog().observe(viewLifecycleOwner){
                    showDialog.value = it
                }
                CalculatorTheme {
                    Surface {
                        Box {
                            Surface(
                                modifier = Modifier
                                    .fillMaxSize()
                                    .systemBarsPadding()
                            ) {
                                Column(modifier = Modifier.fillMaxSize()) {
                                    if (showDialog.value){
                                        Dialog(
                                            onDismissRequest = {showDialog.value = false},
                                            DialogProperties(dismissOnBackPress = false, dismissOnClickOutside = false)
                                        ) {
                                            Box(
                                                contentAlignment= Alignment.Center,
                                                modifier = Modifier.size(dimensionResource(id = R.dimen.img_large))
                                            ) {
                                                CircularProgressIndicator()
                                            }
                                        }
                                    }
                                    TopBar()
                                    Column(horizontalAlignment = Alignment.CenterHorizontally,
                                        modifier = Modifier
                                            .fillMaxSize()
                                            .weight(1f, true)
                                            .padding(vertical = dimensionResource(id = R.dimen.padding_normal))) {
                                        if (calculationHistores.isNotEmpty()){
                                            LazyVerticalGrid(columns = GridCells.Fixed(2)) {
                                                items(calculationHistores.size){
                                                    CardItem(calculationHistores.get(it))
                                                }
                                            }
                                        }else{
                                            Text(getString(R.string.empty_list), fontStyle = FontStyle.Italic,
                                            modifier = Modifier.padding(all = dimensionResource(id = R.dimen.padding_normal)))
                                        }
                                    }
                                    Row(horizontalArrangement = Arrangement.End,
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(horizontal = dimensionResource(id = R.dimen.padding_normal))) {
                                        Button(onClick = {
                                            if (BuildConfig.FLAVOR_engine.equals("filesystem")){
                                                pickPictureLauncher.launch("image/*")
                                            }else{
                                                findNavController().navigate(R.id.navCameraScreenFragment)
                                            }
                                        }) {
                                            Text(getString(R.string.btn_input))
                                        }
                                    }
                                    Divider()
                                    SytemStorage(systemEngine)
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    @Composable
    private fun TopBar(){
        TopAppBar (
            title = { Row(verticalAlignment = Alignment.CenterVertically) {
                Image(
                    painterResource(R.drawable.ic_logo),
                    contentDescription = "Logo",
                    contentScale = ContentScale.Inside,
                    modifier = Modifier
                        .height(dimensionResource(id = R.dimen.icon_small))
                )
                Text(stringResource(R.string.app_name),
                    modifier = Modifier.padding(horizontal = dimensionResource(id = R.dimen.padding_normal)))
            } },
            elevation = 0.dp,
        )
        Divider()
    }

    @Composable
    private fun SytemStorage(selectedItem: MutableState<String?>){
        val radioOptions = listOf("Use file storage", "Use database storage")

        Column(horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
            .fillMaxWidth()
            .selectableGroup()
            .padding(all = dimensionResource(id = R.dimen.padding_normal))) {
            radioOptions.forEach(){label ->
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .selectable(
                            selected = (selectedItem.value == label),
                            onClick = { selectedItem.value = label },
                            role = Role.RadioButton,
                        )
                        .padding(horizontal = dimensionResource(id = R.dimen.padding_normal)),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    RadioButton(
                        modifier = Modifier.padding(end = dimensionResource(id = R.dimen.padding_normal)),
                        selected = (selectedItem.value == label),
                        onClick = { selectedItem.value = label  }
                    )
                    Text(text = label,
                        modifier = Modifier.padding(horizontal = dimensionResource(id = R.dimen.padding_normal)))
                }
            }
            OutlinedButton(onClick = {
                pref.edit().putString(Constants.PREF_STORAGE, selectedItem.value).apply()
                mViewModel.reload()
            },
            enabled = (if(selectedItem.value != pref.getString(Constants.PREF_STORAGE, null)) true else false )) {
                Text(getString(R.string.btn_apply))
            }
        }
    }

    @Composable
    private fun CardItem(calculation: Calculation){
        var bitmap : Bitmap? = null
        try {
            val baseFolder = requireContext().filesDir.absolutePath
            val filePath = Paths.get(baseFolder, calculation.photoFile.id.toString())
            val rawBytes: ByteArray =
                FileEncryptionUtil.decryptFileToMemory(context, filePath.toString())
            bitmap = BitmapFactory.decodeByteArray(rawBytes, 0, rawBytes.size)
        } catch (e: IOException) {
            Timber.e(e)
        } catch (e: GeneralSecurityException) {
            Timber.e(e)
        }
        Card (
            elevation = dimensionResource(id = R.dimen.elevation_normal),
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    horizontal = dimensionResource(R.dimen.padding_normal),
                    vertical = dimensionResource(id = R.dimen.padding_small)
                )
        ){
            Column() {
                if (calculation.photoFile != null){
                    bitmap?.let { Image(bitmap = it.asImageBitmap(), contentDescription = "Image Input",
                        contentScale = ContentScale.FillWidth,
                    modifier = Modifier.fillMaxWidth()) }
                }
                Column(modifier = Modifier
                    .fillMaxWidth()
                    .padding(all = dimensionResource(id = R.dimen.padding_normal))) {
                    Row {
                        Text("Input", modifier = Modifier.weight(1f, true))
                        Text(": " + calculation.input, modifier = Modifier.weight(2f, true))
                    }
                    Row {
                        Text("Result", modifier = Modifier.weight(1f, true))
                        Text(
                            String.format(": %.2f", calculation.result),
                            modifier = Modifier.weight(2f, true)
                        )
                    }
                }
            }
        }
    }

    private fun checkAndRequestStoragePermission() {
        if (PermissionHelper.hasPermission(requireContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
        ) {
            Timber.i("READ_EXTERNAL_STORAGE permission granted")

        } else if (ActivityCompat.shouldShowRequestPermissionRationale(
                requireActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
        ) {
            Timber.i("Permission Rationale is enforced")
            com.google.android.material.snackbar.Snackbar.make(
                requireView(),
                getString(R.string.storage_permissiom_request),
                com.google.android.material.snackbar.Snackbar.LENGTH_INDEFINITE
            )
                .setAction("OK") { v: View? ->
                    Timber.i("Launch permission request screen after showing rationale")
                    storagePermissionLauncher.launch(Manifest.permission.READ_EXTERNAL_STORAGE)
                }
                .show()
        } else {
            Timber.i("Launch permission request screen")
            storagePermissionLauncher.launch(Manifest.permission.READ_EXTERNAL_STORAGE)
        }
    }

    val storagePermissionLauncher = registerForActivityResult<String, Boolean>(
        ActivityResultContracts.RequestPermission()
    ) { isGranted: Boolean ->
        if (isGranted) {
            Timber.i("READ_EXTERNAL_STORAGE permission granted")
            Toast.makeText(context, "Permission granted", Toast.LENGTH_SHORT).show()
        } else {
            //permission was denied
            Toast.makeText(context, "Permission denied...!", Toast.LENGTH_SHORT).show()
        }
    }
}