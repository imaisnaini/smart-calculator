package com.imaisnaini.smartcalculator.ui

import CalculatorTheme
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.imaisnaini.smartcalculator.R
import com.imaisnaini.smartcalculator.ui.theme.Typography
import kotlinx.coroutines.delay

class SpashScreenFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        lifecycleScope.launchWhenCreated {
            delay(3000)
            findNavController().navigate(R.id.spalshScreen_to_home)
        }

        return ComposeView(requireContext()).apply {
            setContent {
                CalculatorTheme {
                    Surface {
                        Box{
                            Surface(modifier = Modifier
                                .fillMaxSize()
                                .systemBarsPadding()
                                .navigationBarsPadding()
                                .imePadding()
                            ) {
                                Column(horizontalAlignment = Alignment.CenterHorizontally,
                                    verticalArrangement = Arrangement.Center,
                                    modifier = Modifier.fillMaxSize()) {
                                    Image(
                                        painterResource(R.drawable.ic_logo),
                                        contentDescription = "Logo",
                                        contentScale = ContentScale.Inside,
                                        modifier = Modifier
                                            .height(dimensionResource(id = R.dimen.img_medium))
                                            .padding(horizontal = dimensionResource(R.dimen.padding_normal))
                                    )
                                    Text(getString(R.string.app_name), style = Typography.h4)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}