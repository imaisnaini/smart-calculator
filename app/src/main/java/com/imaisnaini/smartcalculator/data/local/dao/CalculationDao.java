package com.imaisnaini.smartcalculator.data.local.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.imaisnaini.smartcalculator.data.local.entity.CalculationL;

import java.util.List;
import java.util.UUID;

import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Single;

@Dao
public interface CalculationDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public Completable insert(CalculationL CalculationL);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void blockingInsert(CalculationL CalculationL);

    @Update
    public Completable update(CalculationL CalculationL);

    @Update
    public void blockingUpdate(CalculationL CalculationL);

    @Delete
    public Completable delete(CalculationL CalculationL);

    @Delete
    public void blockingDelete(CalculationL CalculationL);

    @Query("SELECT * FROM t_calculation ORDER BY id desc")
    public Single<List<CalculationL>> findAll();

    @Query("SELECT * FROM t_calculation ORDER BY id desc")
    public List<CalculationL> blockingFindAll();

    @Query("SELECT * FROM t_calculation WHERE id = :id")
    public Single<CalculationL> findById(UUID id);

    @Query("SELECT * FROM t_calculation WHERE id = :id")
    public CalculationL blockingFindById(UUID id);

}
