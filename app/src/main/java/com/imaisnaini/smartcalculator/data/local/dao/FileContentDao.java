package com.imaisnaini.smartcalculator.data.local.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.imaisnaini.smartcalculator.data.local.entity.FileContentL;

import java.util.List;
import java.util.UUID;

import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Single;

@Dao
public interface FileContentDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public Completable insert(FileContentL FileContentL);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void blockingInsert(FileContentL FileContentL);

    @Update
    public Completable update(FileContentL FileContentL);

    @Update
    public void blockingUpdate(FileContentL FileContentL);

    @Delete
    public Completable delete(FileContentL FileContentL);

    @Delete
    public void blockingDelete(FileContentL FileContentL);

    @Query("SELECT * FROM t_file_content ORDER BY id desc")
    public Single<List<FileContentL>> findAll();

    @Query("SELECT * FROM t_file_content ORDER BY id desc")
    public List<FileContentL> blockingFindAll();

    @Query("SELECT * FROM t_file_content WHERE id = :id")
    public Single<FileContentL> findById(UUID id);

    @Query("SELECT * FROM t_file_content WHERE id = :id")
    public FileContentL blockingFindById(UUID id);
}
