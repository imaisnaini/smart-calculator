package com.imaisnaini.smartcalculator.data.local.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.github.f4b6a3.uuid.UuidCreator;

import java.util.Date;
import java.util.UUID;

@Entity(tableName = "t_device_log")
public class DeviceLogL {

    public static final String EV_INSTALL = "INSTALL";
    public static final String EV_CRASH = "CRASH";

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "id")
    public UUID id;
    @ColumnInfo(name = "application_id")
    public String applicationId;
    @ColumnInfo(name = "git_hash")
    public String gitHash;
    @ColumnInfo(name = "installation_id")
    public UUID installationId;
    @ColumnInfo(name = "event_type")
    public String eventType;
    @ColumnInfo(name = "description")
    public String description;

    @ColumnInfo(name = "created_by")
    public String createdBy;
    @ColumnInfo(name = "created_time")
    public Date createdTime;
    @ColumnInfo(name = "updated_by")
    public String updatedBy;
    @ColumnInfo(name = "updated_time")
    public Date updatedTime;
    @ColumnInfo(name = "deleted_by")
    public String deletedBy;
    @ColumnInfo(name = "deleted_time")
    public Date deletedTime;
    @ColumnInfo(name = "synced_status")
    public int syncedStatus;
    @ColumnInfo(name = "synced_time")
    public Date syncedTime;
    @ColumnInfo(name = "is_deleted")
    public int isDeleted;
    @ColumnInfo(name = "version")
    public int version;

    public DeviceLogL() {
        id = UuidCreator.getTimeOrdered();
        Date now = new Date();
        createdBy = "SYSTEM";
        updatedBy = "SYSTEM";
        createdTime = now;
        updatedTime = now;
        syncedStatus = 0;
        isDeleted = 0;
        version = 1;
    }

    @Ignore
    public DeviceLogL(String applicationId, String gitHash) {
        id = UuidCreator.getTimeOrdered();
        Date now = new Date();
        this.applicationId = applicationId;
        this.gitHash = gitHash;
        createdBy = "SYSTEM";
        updatedBy = "SYSTEM";
        createdTime = now;
        updatedTime = now;
        syncedStatus = 0;
        isDeleted = 0;
        version = 1;
    }

}
