package com.imaisnaini.smartcalculator.data.domain.entity;

import com.github.f4b6a3.uuid.UuidCreator;

import java.util.Date;
import java.util.UUID;

public class Calculation {
    public UUID id;
    public String input;
    public Double result;
    public FileContent photoFile;

    public Date createdTime;
    public String createdBy;
    public Date updatedTime;
    public String updatedBy;
    public Date deletedTime;
    public String deletedBy;
    public int syncedStatus;
    public Date syncedTime;
    public int isDeleted;
    public int version;

    public Calculation() {
        id = UuidCreator.getTimeOrdered();
        Date now = new Date();
        createdTime = now;
        updatedTime = now;
        syncedStatus = 0;
        isDeleted = 0;
        version = 1;
    }
}
