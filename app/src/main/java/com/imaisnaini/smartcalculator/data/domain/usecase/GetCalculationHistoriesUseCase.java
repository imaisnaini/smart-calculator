package com.imaisnaini.smartcalculator.data.domain.usecase;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.imaisnaini.smartcalculator.Constants;
import com.imaisnaini.smartcalculator.SmartCalculatorApplication;
import com.imaisnaini.smartcalculator.data.PagingResult;
import com.imaisnaini.smartcalculator.data.domain.entity.Calculation;
import com.imaisnaini.smartcalculator.data.domain.repository.CalculationRepository;

import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class GetCalculationHistoriesUseCase {
    private SmartCalculatorApplication app;
    private CalculationRepository calculationRepository;

    @Inject
    public GetCalculationHistoriesUseCase(Application app, CalculationRepository calculationRepository) {
        this.app = (SmartCalculatorApplication) app;
        this.calculationRepository = calculationRepository;
    }

    public PagingResult<Calculation> invokeSync(){
        SharedPreferences pref = app.getSharedPreferences(Constants.PREFERENCE_FILE_NAME, Context.MODE_PRIVATE);
        UUID installationID = UUID.fromString(pref.getString(Constants.PREF_INSTALLATION_UUID, null));
        if (pref.getString(Constants.PREF_STORAGE, null).equals("Use file storage")){
            return calculationRepository.blockingFindAllMemory();
        }else {
            return calculationRepository.blockingFindAll();
        }
    }
}
