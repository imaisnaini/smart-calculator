package com.imaisnaini.smartcalculator.data.local;

import static com.imaisnaini.smartcalculator.Constants.DATABASE_NAME;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.AutoMigration;
import androidx.room.Database;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

import com.imaisnaini.smartcalculator.BuildConfig;
import com.imaisnaini.smartcalculator.data.local.converter.BooleanConverter;
import com.imaisnaini.smartcalculator.data.local.converter.DateConverter;
import com.imaisnaini.smartcalculator.data.local.converter.LocalDateConverter;
import com.imaisnaini.smartcalculator.data.local.converter.UuidConverter;
import com.imaisnaini.smartcalculator.data.local.dao.CalculationDao;
import com.imaisnaini.smartcalculator.data.local.dao.DeviceLogDao;
import com.imaisnaini.smartcalculator.data.local.dao.FileContentDao;
import com.imaisnaini.smartcalculator.data.local.entity.CalculationL;
import com.imaisnaini.smartcalculator.data.local.entity.DeviceLogL;
import com.imaisnaini.smartcalculator.data.local.entity.FileContentL;
import com.imaisnaini.smartcalculator.util.FileEncryptionUtil;

import net.sqlcipher.database.SupportFactory;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {CalculationL.class, FileContentL.class, DeviceLogL.class},
        version = 1, exportSchema = true,
        autoMigrations = {})
@TypeConverters({DateConverter.class, BooleanConverter.class, UuidConverter.class, LocalDateConverter.class})
public abstract class AppDatabase extends RoomDatabase {
    private static volatile AppDatabase INSTANCE;
    public static final int NUMBER_OF_THREADS = 2;
    public static final ExecutorService executor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public abstract CalculationDao calculationDao();
    public abstract FileContentDao fileContentDao();
    public abstract DeviceLogDao deviceLogDao();

    public static AppDatabase getInstance(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    Builder<AppDatabase> builder = Room.databaseBuilder(context.getApplicationContext(),
                                    AppDatabase.class, DATABASE_NAME)
                            .addCallback(sRoomDatabaseCallback);

                    if(!BuildConfig.DEBUG) {
                        try {
                            byte[] passphrase = getPassphrase(context);
                            builder.openHelperFactory(new SupportFactory(passphrase));
                        } catch (GeneralSecurityException | IOException exc) {
                            throw new RuntimeException("Failed while preparing DB encryption", exc);
                        }
                    }
                    INSTANCE = builder.build();
                }
            }
        }
        return INSTANCE;
    }

    private static Callback sRoomDatabaseCallback = new Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
        }
    };

    private static byte[] getPassphrase(Context context) throws GeneralSecurityException, IOException {
        byte[] passphrase = null;
        File keyFile = context.getDatabasePath(DATABASE_NAME + ".key");
        if(keyFile.exists()) {
            passphrase = FileEncryptionUtil.decryptFileToMemory(context, keyFile.getAbsolutePath());
        } else {
            passphrase = generatePassphrase(context);
        }
        return passphrase;
    }

    private static byte[] generatePassphrase(Context context) throws GeneralSecurityException, IOException {
        SecureRandom sr = null;
        try {
            sr = SecureRandom.getInstanceStrong();
        } catch (NoSuchAlgorithmException e) {
            sr = new SecureRandom();
        }

        byte[] buffer = new byte[32];
        sr.nextBytes(buffer);
        while(Arrays.asList(buffer).contains(0)) {
            sr.nextBytes(buffer);
        }
        FileEncryptionUtil.encryptMemoryToFile(context, buffer, context.getDatabasePath(DATABASE_NAME + ".key").getAbsolutePath());
        return buffer;
    }
}
