package com.imaisnaini.smartcalculator.data.domain.repository;

import android.content.Context;

import com.imaisnaini.smartcalculator.data.SingleResult;
import com.imaisnaini.smartcalculator.data.domain.entity.FileContent;
import com.imaisnaini.smartcalculator.data.local.dao.FileContentDao;
import com.imaisnaini.smartcalculator.data.local.entity.FileContentL;
import com.imaisnaini.smartcalculator.data.mapper.FileContentMapper;

import org.mapstruct.factory.Mappers;

import java.util.UUID;
import java.util.function.Consumer;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.hilt.android.qualifiers.ApplicationContext;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;
import timber.log.Timber;

public class FileContentRepository {
    private Context context;
    private FileContentDao fileContentDao;

    @Inject
    public FileContentRepository(@ApplicationContext Context context, FileContentDao fileContentDao) {
        this.context = context;
        this.fileContentDao = fileContentDao;
    }

    public void insert(FileContent value, Consumer<SingleResult<FileContent>> callback){
        Single.fromCallable(() -> blockingInsert(value))
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(result -> {
                    if(callback != null) {
                        callback.accept(result);
                    }
                }, throwable -> {
                    Timber.e(throwable);
                    if(callback != null) {
                        SingleResult<FileContent> sr = new SingleResult<FileContent>();
                        sr.setSuccess(false);
                        sr.setMessage(throwable.getMessage());
                        callback.accept(sr);
                    }
                });
    }

    public SingleResult<FileContent> blockingInsert(FileContent value) {
        SingleResult<FileContent> toReturn = new SingleResult<FileContent>();
        FileContentMapper mapper = Mappers.getMapper(FileContentMapper.class);
        FileContentL localRecord = mapper.fromDomainToLocal(value);
        try {
            fileContentDao.blockingInsert(localRecord);
        } catch (Exception exc) {
            Timber.e(exc);
            toReturn.setSuccess(false);
            toReturn.setMessage(exc.getMessage());
        }
        return toReturn;
    }
}
