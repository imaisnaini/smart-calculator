package com.imaisnaini.smartcalculator.data;

import java.util.Date;

public class SingleResult<T> {

    private boolean success;
    private String message;
    private T record;
    private Date serverTime;

    public boolean getSuccess() { return success;} public void setSuccess(boolean val) { success = val;}
    public String getMessage() { return message;} public void setMessage(String val) { message = val;}
    public T getRecord() { return record; } public void setRecord(T val) { record = val;}
    public Date getServerTime() { return serverTime;} public void setServerTime(Date val) { serverTime = val; }

    public SingleResult()
    {
        message = "OK";
        success = true;
        serverTime = new Date();
    }

    public void copyMetadataFrom(SingleResult<?> source) {
        success = source.getSuccess();
        message = source.getMessage();
        serverTime = source.getServerTime();
    }
}
