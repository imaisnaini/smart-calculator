package com.imaisnaini.smartcalculator.data.local.converter;

import androidx.room.TypeConverter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class LocalDateConverter {
    @TypeConverter
    public static LocalDate fromString(String value) {
        try {
            if(value != null) {
                return LocalDate.parse(value);
            }
        }
        catch ( DateTimeParseException e ) { }
        return null;
    }

    @TypeConverter
    public static String toString(LocalDate value) {
        if(value != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            return formatter.format(value);
        }
        return null;
    }
}
