package com.imaisnaini.smartcalculator.data.local.converter;

import androidx.room.TypeConverter;

import java.util.UUID;

public class UuidConverter {
    @TypeConverter
    public static UUID fromString(String value) {
        return value == null ? null : UUID.fromString(value);
    }

    @TypeConverter
    public static String toString(UUID val) {
        return val == null ? null : val.toString();
    }
}
