package com.imaisnaini.smartcalculator.data.mapper;



import com.imaisnaini.smartcalculator.data.domain.entity.Calculation;
import com.imaisnaini.smartcalculator.data.local.entity.CalculationL;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.UUID;

@Mapper
public interface CalculationMapper {
    @Mapping(ignore = true, target="photoFile")
    Calculation fromLocalToDomain(CalculationL source);

    @Mapping(source="photoFile.id", target="photoFileId")
    @Mapping(source="photoFile.localRelativePath", target="photoFilePath")
    CalculationL fromDomainToLocal(Calculation source);

}
