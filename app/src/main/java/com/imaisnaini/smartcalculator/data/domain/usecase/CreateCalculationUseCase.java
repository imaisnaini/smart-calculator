package com.imaisnaini.smartcalculator.data.domain.usecase;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;

import androidx.security.crypto.EncryptedFile;

import com.github.f4b6a3.uuid.UuidCreator;
import com.imaisnaini.smartcalculator.Constants;
import com.imaisnaini.smartcalculator.SmartCalculatorApplication;
import com.imaisnaini.smartcalculator.data.SingleResult;
import com.imaisnaini.smartcalculator.data.domain.entity.Calculation;
import com.imaisnaini.smartcalculator.data.domain.entity.FileContent;
import com.imaisnaini.smartcalculator.data.domain.repository.CalculationRepository;
import com.imaisnaini.smartcalculator.data.domain.repository.FileContentRepository;
import com.imaisnaini.smartcalculator.data.local.AppDatabase;
import com.imaisnaini.smartcalculator.util.FileEncryptionUtil;
import com.imaisnaini.smartcalculator.util.StringConverterUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Singleton;

import timber.log.Timber;

@Singleton
public class CreateCalculationUseCase {
    private SmartCalculatorApplication app;
    private CalculationRepository calculationRepository;
    private FileContentRepository fileContentRepository;
    private final DateFormat dateFormatForFileName = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ROOT);

    @Inject
    public CreateCalculationUseCase(Application app, CalculationRepository calculationRepository, FileContentRepository fileContentRepository) {
        this.app = (SmartCalculatorApplication) app;
        this.calculationRepository = calculationRepository;
        this.fileContentRepository = fileContentRepository;
    }

    public SingleResult<Calculation> invokeSync(Bitmap image, String textOutput){
        SingleResult<Calculation> toReturn = new SingleResult<>();
        SharedPreferences pref = app.getSharedPreferences(Constants.PREFERENCE_FILE_NAME, Context.MODE_PRIVATE);
        UUID installationID = UUID.fromString(pref.getString(Constants.PREF_INSTALLATION_UUID, null));
        FileOutputStream fos = null;
        Calculation calculation = new Calculation();

        textOutput = textOutput.replace(" ", ""); // remove white spcae
        textOutput = textOutput.toLowerCase();
        // handle missed reading the math operator
        textOutput = textOutput.replace("t", "+");
        textOutput = textOutput.replace("_", "-");
        textOutput = textOutput.replace("x", "*");
        textOutput = textOutput.replace(":", "/");
        Timber.i("textOutput : " + textOutput);
        String[] firstSplit = textOutput.split("(?=[\\*+-/])");
        Timber.i("split length : %d", firstSplit.length);

        //checking if the text splited contain math operator
        if (firstSplit.length >= 2) {
            Double firstVar = StringConverterUtil.str2double(firstSplit[0]);
            String operator = firstSplit[1].substring(0,1);
            Double secondVar = StringConverterUtil.str2double(firstSplit[1].substring(1));
            double result = 0d;
            switch(operator){
                case "+":
                    result = firstVar + secondVar;
                    break;
                case "-":
                    result = firstVar - secondVar;
                    break;
                case "*":
                    result = firstVar * secondVar;
                    break;
                case "/" :
                    result = firstVar / secondVar;
                    break;
            }
            calculation.input = textOutput;
            calculation.result = result;
        }else {
            toReturn.setSuccess(false);
            toReturn.setMessage("Invalid input. Input must contain simple math operation.");
        }
        try {
            // save bitmap to file
            File outputDirectory = app.getApplicationContext().getFilesDir();
            UUID fileId = UuidCreator.getTimeOrdered();
            File outputFile = new File(outputDirectory, fileId.toString());
            Path outputPath = outputFile.toPath();

            EncryptedFile encryptedFile = FileEncryptionUtil.obtainEncryptedFile(app.getApplicationContext(), outputPath.toString());
            fos = encryptedFile.openFileOutput();
            image.compress(Bitmap.CompressFormat.JPEG, 75, fos);
            fos.flush();
            long fileSize = Files.size(outputPath);

            // create FileContent
            Date now = new Date();
            String publicFilename = String.format("IMG_%s.jpg", dateFormatForFileName.format(now));

            FileContent photoFile = new FileContent();
            photoFile.id = fileId;
            photoFile.localRelativePath = fileId.toString();
            photoFile.filename = publicFilename;
            photoFile.sizeByte = (int)fileSize;
            photoFile.mimeType = "image/jpeg";

            calculation.photoFile = photoFile;

            // saving to db, run in single transaction
            AppDatabase db = AppDatabase.getInstance(app.getApplicationContext());
            db.runInTransaction(() -> {
                if (pref.getString(Constants.PREF_STORAGE, null).equals("Use file storage")){
                    calculationRepository.blockingInsertMemory(calculation);
                }else {
                    fileContentRepository.blockingInsert(photoFile);
                    calculationRepository.blockingInsert(calculation);
                }
            });

            toReturn.setRecord(calculation);
        } catch (Exception exc) {
            toReturn.setSuccess(false);
            toReturn.setMessage(exc.getMessage());
        } finally {
            if(fos != null) {
                try {
                    fos.close();
                } catch (IOException exc) {
                    Timber.e(exc);
                }
            }
        }
        return toReturn;
    }
}
