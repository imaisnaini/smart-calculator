package com.imaisnaini.smartcalculator.data.domain.repository;

import static com.imaisnaini.smartcalculator.Constants.DATABASE_NAME;
import static com.imaisnaini.smartcalculator.Constants.STORAGE_NAME;

import android.content.Context;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.imaisnaini.smartcalculator.data.PagingResult;
import com.imaisnaini.smartcalculator.data.SingleResult;
import com.imaisnaini.smartcalculator.data.domain.entity.Calculation;
import com.imaisnaini.smartcalculator.data.domain.entity.FileContent;
import com.imaisnaini.smartcalculator.data.local.dao.CalculationDao;
import com.imaisnaini.smartcalculator.data.local.entity.CalculationL;
import com.imaisnaini.smartcalculator.data.mapper.CalculationMapper;
import com.imaisnaini.smartcalculator.util.FileEncryptionUtil;

import org.mapstruct.factory.Mappers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.hilt.android.qualifiers.ApplicationContext;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;
import timber.log.Timber;

@Singleton
public class CalculationRepository {
    private CalculationDao calculationDao;
    private Context context;
    private Map<UUID, Calculation> inMemoryCalculation;
    private ObjectMapper om;

    @Inject
    public CalculationRepository(
            @ApplicationContext Context context, CalculationDao calculationDao) {
        this.context = context;
        this.calculationDao = calculationDao;
        om = new ObjectMapper();
        loadCalculationRecords();
    }

    public void insert(Calculation value, Consumer<SingleResult<Calculation>> callback){
        Single.fromCallable(() -> blockingInsertMemory(value))
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(result -> {
                    if(callback != null) {
                        callback.accept(result);
                    }
                }, throwable -> {
                    Timber.e(throwable);
                    if(callback != null) {
                        SingleResult<Calculation> sr = new SingleResult<Calculation>();
                        sr.setSuccess(false);
                        sr.setMessage(throwable.getMessage());
                        callback.accept(sr);
                    }
                });
    }

    public SingleResult<Calculation> blockingInsert(Calculation value) {
        SingleResult<Calculation> toReturn = new SingleResult<Calculation>();
        CalculationMapper mapper = Mappers.getMapper(CalculationMapper.class);
        CalculationL localRecord = mapper.fromDomainToLocal(value);
        try {
            calculationDao.blockingInsert(localRecord);
        } catch (Exception exc) {
            Timber.e(exc);
            toReturn.setSuccess(false);
            toReturn.setMessage(exc.getMessage());
        }
        return toReturn;
    }

    public SingleResult<Calculation> blockingInsertMemory(Calculation value) {
        SingleResult<Calculation> toReturn = new SingleResult<Calculation>();
        CalculationMapper mapper = Mappers.getMapper(CalculationMapper.class);
        CalculationL localRecord = mapper.fromDomainToLocal(value);
        try {
            inMemoryCalculation.put(value.id, value);
            saveCalculationRecords();
            saveCalculationRecordsBin();
        } catch (Exception exc) {
            Timber.e(exc);
            toReturn.setSuccess(false);
            toReturn.setMessage(exc.getMessage());
        }
        return toReturn;
    }
    public void findAll(Consumer<PagingResult<Calculation>> callback){
        Single.fromCallable(() -> blockingFindAll())
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(result -> {
                    if(callback != null) {
                        callback.accept(result);
                    }
                }, throwable -> {
                    Timber.e(throwable);
                    if(callback != null) {
                        PagingResult<Calculation> sr = new PagingResult<Calculation>();
                        sr.setSuccess(false);
                        sr.setMessage(throwable.getMessage());
                        callback.accept(sr);
                    }
                });
    }

    public PagingResult<Calculation> blockingFindAll() {
        PagingResult<Calculation> toReturn = new PagingResult<Calculation>();
        CalculationMapper mapper = Mappers.getMapper(CalculationMapper.class);
        try {
            List<CalculationL> localRecords = calculationDao.blockingFindAll();
            List<Calculation> domainRecords = localRecords.stream().map(x ->{
                Calculation domain = mapper.fromLocalToDomain(x);
                if (x.photoFileId != null){
                    FileContent content = new FileContent();
                    content.id = x.photoFileId;
                    domain.photoFile = content;
                }
                return domain;
            }).collect(Collectors.toList());
            toReturn.setRecords(domainRecords);
            toReturn.setTotalRecords(localRecords.size());
        } catch (Exception exc) {
            Timber.e(exc);
            toReturn.setSuccess(false);
            toReturn.setMessage(exc.getMessage());
        }
        return toReturn;
    }

    public PagingResult<Calculation> blockingFindAllMemory() {
        PagingResult<Calculation> toReturn = new PagingResult<Calculation>();
        CalculationMapper mapper = Mappers.getMapper(CalculationMapper.class);
        try {
            List<Calculation> domainRecords = new ArrayList<>(inMemoryCalculation.values());
            toReturn.setRecords(domainRecords);
            toReturn.setTotalRecords(domainRecords.size());
        } catch (Exception exc) {
            Timber.e(exc);
            toReturn.setSuccess(false);
            toReturn.setMessage(exc.getMessage());
        }
        return toReturn;
    }

    private void loadCalculationRecords() {
        try {
            File filesDir = context.getFilesDir();
            Path storage = Paths.get(filesDir.toString(), "calculation.json");

            if(Files.exists(storage)) {
                TypeReference<HashMap<UUID, Calculation>> typeRef
                        = new TypeReference<HashMap<UUID, Calculation>>() {};
                inMemoryCalculation = om.readValue(storage.toFile(), typeRef);
            } else {
                inMemoryCalculation = new HashMap<>();
            }
        } catch (IOException exc) {
            Timber.e(exc);
            inMemoryCalculation = new HashMap<>();
        }
    }

    private void saveCalculationRecords() {
        try {
            File filesDir = context.getFilesDir();
            Path storage = Paths.get(filesDir.toString(), "calculation.json");
            om = new ObjectMapper();
            om.writeValue(storage.toFile(), inMemoryCalculation);
        } catch (IOException exc) {
            Timber.e(exc);
        }
    }


    // https://www.geeksforgeeks.org/how-to-serialize-hashmap-in-java/
    private void saveCalculationRecordsBin() {
        File filesDir = context.getFilesDir();
        Path storage = Paths.get(filesDir.toString(), "calculation.bin");
        String transformation = "AES/PKCS5Padding";
        byte[] passphrase = null;
        try {
            passphrase = getPassphrase(context);
        } catch (GeneralSecurityException | IOException e) {
            e.printStackTrace();
        }
        SecretKeySpec sks = new SecretKeySpec(passphrase, transformation);
        // Create cipher
        Cipher cipher = null;
        try {
            cipher = Cipher.getInstance(transformation);
            cipher.init(Cipher.ENCRYPT_MODE, sks);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException e) {
            e.printStackTrace();
        }

        try (FileOutputStream fos = new FileOutputStream(storage.toFile());
             CipherOutputStream cos = new CipherOutputStream(fos, cipher);
             ObjectOutputStream oos = new ObjectOutputStream(cos)){
            oos.writeObject(inMemoryCalculation);
        }
        catch (IOException e) {
            Timber.e(e);
        }
    }
    private static byte[] getPassphrase(Context context) throws GeneralSecurityException, IOException {
        byte[] passphrase = null;
        File keyFile = context.getDatabasePath(STORAGE_NAME + ".key");
        if(keyFile.exists()) {
            passphrase = FileEncryptionUtil.decryptFileToMemory(context, keyFile.getAbsolutePath());
        } else {
            passphrase = generatePassphrase(context);
        }
        return passphrase;
    }

    private static byte[] generatePassphrase(Context context) throws GeneralSecurityException, IOException {
        SecureRandom sr = null;
        try {
            sr = SecureRandom.getInstanceStrong();
        } catch (NoSuchAlgorithmException e) {
            sr = new SecureRandom();
        }

        byte[] buffer = new byte[32];
        sr.nextBytes(buffer);
        while(Arrays.asList(buffer).contains(0)) {
            sr.nextBytes(buffer);
        }
        FileEncryptionUtil.encryptMemoryToFile(context, buffer, context.getDatabasePath(STORAGE_NAME + ".key").getAbsolutePath());
        return buffer;
    }
}
