package com.imaisnaini.smartcalculator.data;

import java.util.Date;
import java.util.List;

public class PagingResult<T> {

    private boolean success;
    private String message;
    private int page;
    private long totalRecords;
    private List<T> records;
    private Date serverTime;

    public boolean getSuccess() { return success;}
    public String getMessage() { return message;}
    public int getPage() { return page;}
    public long getTotalRecords() { return totalRecords;}
    public List<T> getRecords() { return records;}
    public Date getServerTime() { return serverTime;}

    public void setSuccess(boolean val) { success = val;}
    public void setMessage(String val) { message = val;}
    public void setPage(int val) { page = val;}
    public void setTotalRecords(long val) { totalRecords = val;}
    public void setRecords(List<T> val) { records = val;}
    public void setServerTime(Date val) { serverTime = val;}

    public PagingResult()
    {
        totalRecords = 0L;
        page = 0;
        message = "OK";
        success = true;
        serverTime = new Date();
    }

    public void copyMetadataFrom(PagingResult<?> source) {
        success = source.getSuccess();
        message = source.getMessage();
        page = source.getPage();
        totalRecords = source.getTotalRecords();
        serverTime = source.getServerTime();
    }
}
