package com.imaisnaini.smartcalculator.data.mapper;

import com.imaisnaini.smartcalculator.data.domain.entity.FileContent;
import com.imaisnaini.smartcalculator.data.local.entity.FileContentL;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.UUID;

@Mapper
public abstract class FileContentMapper {

    public abstract FileContent fromLocalToDomain(FileContentL source);

    public abstract FileContentL fromDomainToLocal(FileContent source);


    String map(UUID val) {
        return val.toString();
    }

    UUID map(String val) {
        return UUID.fromString(val);
    }
}
