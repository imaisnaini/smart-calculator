package com.imaisnaini.smartcalculator.data.local.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.github.f4b6a3.uuid.UuidCreator;

import java.util.Date;
import java.util.UUID;

@Entity(tableName = "t_file_content")
public class FileContentL {
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "id")
    public UUID id;
    @ColumnInfo(name = "filename")
    public String filename;
    @ColumnInfo(name = "local_relative_path")
    public String localRelativePath;
    @ColumnInfo(name = "mime_type")
    public String mimeType;
    @ColumnInfo(name = "size_byte")
    public int sizeByte;

    @ColumnInfo(name = "created_by")
    public String createdBy;
    @ColumnInfo(name = "created_time")
    public Date createdTime;
    @ColumnInfo(name = "updated_by")
    public String updatedBy;
    @ColumnInfo(name = "updated_time")
    public Date updatedTime;
    @ColumnInfo(name = "deleted_by")
    public String deletedBy;
    @ColumnInfo(name = "deleted_time")
    public Date deletedTime;
    @ColumnInfo(name = "synced_status")
    public int syncedStatus;
    @ColumnInfo(name = "synced_time")
    public Date syncedTime;
    @ColumnInfo(name = "is_deleted")
    public int isDeleted;
    @ColumnInfo(name = "version")
    public int version;

    public FileContentL() {
        id = UuidCreator.getTimeOrdered();
        Date now = new Date();
        createdTime = now;
        updatedTime = now;
        syncedStatus = 0;
        isDeleted = 0;
        version = 1;
    }
}
