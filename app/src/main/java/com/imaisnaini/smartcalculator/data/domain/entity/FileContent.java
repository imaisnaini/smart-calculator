package com.imaisnaini.smartcalculator.data.domain.entity;

import java.util.Date;
import java.util.UUID;

public class FileContent {
    public UUID id;
    public String filename;
    public String mimeType;
    public String localRelativePath;
    public int sizeByte;

    public Date createdTime;
    public String createdBy;
    public Date updatedTime;
    public String updatedBy;
    public Date deletedTime;
    public String deletedBy;
    public int syncedStatus;
    public Date syncedTime;
    public int isDeleted;
    public int version;

    public FileContent() {
        Date now = new Date();
        createdTime = now;
        updatedTime = now;
        syncedStatus = 0;
        isDeleted = 0;
        version = 1;
    }
}
