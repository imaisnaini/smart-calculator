package com.imaisnaini.smartcalculator.data.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.github.f4b6a3.uuid.UuidCreator;

import java.util.Date;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DeviceLog {

	public UUID id;
	public String applicationId;
	public String gitHash;
	public UUID installationId;
	public UUID userId;
	public String eventType;
	public String description;

	public Date createdTime;
	public String createdBy;
	public Date updatedTime;
	public String updatedBy;
	public Date deletedTime;
	public String deletedBy;
	public Date syncedTime;
	public int syncedStatus;
	public int version;
	public int isDeleted;

	public DeviceLog() {
		id = UuidCreator.getTimeOrdered();
		Date now = new Date();
		createdTime = now;
		updatedTime = now;
		syncedStatus = 0;
		isDeleted = 0;
		version = 1;
	}
}
