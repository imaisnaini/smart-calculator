package com.imaisnaini.smartcalculator.data.local.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.imaisnaini.smartcalculator.data.local.entity.DeviceLogL;

import java.util.Date;
import java.util.UUID;

@Dao
public interface DeviceLogDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void blockingInsert(DeviceLogL deviceLog);

    @Update
    public void blockingUpdate(DeviceLogL deviceLog);

    @Query("SELECT * FROM t_device_log WHERE id = :id")
    DeviceLogL blockingFindById(UUID id);

    @Query("update t_device_log set synced_status = :syncStatus, synced_time = :syncTime where id = :id")
    void blockingUpdateSyncStatus(UUID id, int syncStatus, Date syncTime);

}
