package com.imaisnaini.smartcalculator.data.local.converter;
import androidx.room.TypeConverter;

public class BooleanConverter {
    @TypeConverter
    public static Boolean fromInt(Integer value) {
        return value == null ? null : !value.equals(0);
    }

    @TypeConverter
    public static Integer toInt(Boolean val) {
        return val == null ? null : (val ? 1 : 0);
    }
}
