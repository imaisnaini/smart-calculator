
# Smart Calculator





[![release version](https://img.shields.io/badge/release-1.0.0-blue)](https://gitlab.com/imaisnaini/smart-calculator/-/commits/1.0.0)



## About

Scan me! calculator, a configurable image-to-result calculator.


## Screenshots

![App Screenshot](https://i.postimg.cc/fb191sLt/Screenshot-2023-05-14-235351.png)
![App Screenshot](https://i.postimg.cc/HLNhKrXw/Screenshot-2023-05-17-132439.png)

